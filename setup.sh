#!/bin/bash
# General setup for a new server, use with care, do not trust the internet.
# Execute as root
# curl -o ~/setup.sh https://gitlab.com/benoit.lavorata/dct/raw/master/setup.sh && chmod +x ~/setup.sh  && bash ~/setup.sh

cd ~

echo "[INFO] Upgrade and general dependencies"
apt-get update
apt-get upgrade -y
apt-get install -y curl jq git
echo "[OK] Installed dependencies"

echo "[INFO] Installing Docker"
curl -fsSL https://get.docker.com -o install-docker.sh
sh install-docker.sh
echo "[OK] Installed Docker"


echo "[INFO] Installing DCT"
mkdir ~/bin
curl -o ~/bin/dct.sh https://gitlab.com/benoit.lavorata/dct/raw/master/dct.sh
chmod +x ~/bin/dct.sh 
echo "" >> $HOME/.bashrc && echo 'alias dct="bash ~/bin/dct.sh"' >> $HOME/.bashrc && source $HOME/.bashrc
echo "[OK] Installed DCT"

echo "Installation finished, you may want to run this"
echo "rm setup.sh"
echo "mkdir ~/apps"
echo "cd ~/apps"
echo "dct nginx"
echo "dct nodered"
echo "cd nginx"
echo "./up.sh"